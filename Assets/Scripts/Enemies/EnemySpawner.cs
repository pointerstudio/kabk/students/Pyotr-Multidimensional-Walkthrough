using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private float spawnOffset = 10f; // JAKOB: also this can be simply public

    public GameObject enemyToSpawn;
    public int numberOfEnemies = 5; // JAKOB: unused, you can get rid of it

    void Start()
    {
        Transform.Instantiate(enemyToSpawn, new Vector3(0, 2.5f, 0), Quaternion.identity);
    }

    public void AwakeTheEvil()
    {
        float newZ = transform.position.z + spawnOffset;
        Vector3 newRegion = transform.position = new Vector3(0, 2.5f, newZ); // JAKOB: better break this in two lines
                                                                             // multi assignments can be harder to read
                                                                             // functionally this is absolutely fine though

        Transform.Instantiate(enemyToSpawn, newRegion, Quaternion.identity);
    }
}
