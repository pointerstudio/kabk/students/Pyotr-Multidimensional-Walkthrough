using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// JAKOB:
// the ScoreSystem could be something perfect for a static class
// like PowerEconomy. It should be globally available, and always
// give back the same result. It's kind of a meta-property right?
//
//
public class ScoreSystem : MonoBehaviour
{
    EnemyScript enemyScript;
    PlayerScript playerScript;

    // int currentScore = 0;
    // int scoreMultiplier = 10; // will be changed according to dimension

    void Awake()
    {
        enemyScript = GetComponent<EnemyScript>();
        //int currentScore = 0;
    }

    // JAKOB: I wouldn't make Start() private, since it's a Unity defined
    // function. You can check here which functions are predefined:
    // https://docs.unity3d.com/ScriptReference/MonoBehaviour.html
    //
    // Why be careful with "private" especially? Unity has internally a
    // system to call these predefined functions. This system would
    // possibly not be able to call these functions from outside, if they
    // are private. "outside" is anything that is not within this class
    // (or more simply thought, anything that is not within this textfile.)
    // It may still be able to call it through some magic, but I wouldn't
    // risk it if it's not really necessary, as this could also change per
    // Unity version or platform. E.g. it could work in the editor, but not
    // when you export it.
    private void Start()
    {
        // int currentScore = 0;
        // int scoreMultiplier = 10; // will be changed according to dimension
    }

    void Update()
    {
        // int currentScore = playerScript.deadEnemies * playerScript.scoreMultiplier;

        //Debug.Log("score: " + currentScore);
        //Debug.Log("killed enemies: " + enemyScript.deadEnemies);
        //Debug.Log("score: " + (enemyScript.deadEnemies * scoreMultiplier));
    }
}
