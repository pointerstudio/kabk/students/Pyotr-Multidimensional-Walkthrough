using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChange : MonoBehaviour
{
    public Camera cam1D;
    public Camera cam2D;
    public Camera cam3D;

    PlayerScript playerScript;          // JAKOB: my guess is, that you want to
                                        // get some data from playerScript and
                                        // use it to change camera?
                                        // then you need to make it public, and
                                        // link the object with the attached PlayerScript
                                        // in the editor. afaik this would be "Model"
                                        //
                                        // then you can access all its public variables like
                                        //  if (cam1D.gameObject.activeSelf) {
                                        //      // in conditionals "&&" means AND and "||" means OR
                                        //      // so like this you can trigger the camera change both
                                        //      // on keypress and automatically based on e.g. deadEnemies
                                        //      if (Input.GetKeyDown("tab") || playerScript.deadEnemies > 5) {
                                        //          cam1D.gameObject.SetActive(false);
                                        //          cam2D.gameObject.SetActive(true);
                                        //          cam3D.gameObject.SetActive(false);
                                        //      }
                                        //  }
                                        //
                                        // it would be possible to not use two if-statements,
                                        // but this may be the option which is most readable.
                                        // you thank yourself later if you put readability above elegance
                                        //
                                        // just noticed that you do exactly this with other scripts,
                                        // leaving the comment in anyways :-)

    //int yes = playerScript.scoreMultiplier;

    void Start()
    {
        // set first camera
        cam1D.gameObject.SetActive(true);
        cam2D.gameObject.SetActive(false);
        cam3D.gameObject.SetActive(false);
    }

    void Update()
    {
        // change camera on input
        if (cam1D.gameObject.activeSelf && Input.GetKeyDown("tab"))
        {
            cam1D.gameObject.SetActive(false);
            cam2D.gameObject.SetActive(true);
            cam3D.gameObject.SetActive(false);

            Debug.Log("1D -> 2D happened");
        }
        // change camera on input
        else if (cam2D.gameObject.activeSelf && Input.GetKeyDown("tab"))
        {
            cam1D.gameObject.SetActive(false);
            cam2D.gameObject.SetActive(false);
            cam3D.gameObject.SetActive(true);

            Debug.Log("2D -> 3D happened");
        }
        // change camera on input
        else if (cam3D.gameObject.activeSelf && Input.GetKeyDown("tab"))
        {
            cam1D.gameObject.SetActive(true);
            cam2D.gameObject.SetActive(false);
            cam3D.gameObject.SetActive(false);

            Debug.Log("3D -> 1D happened");
        }
    }
}
