using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FloorSpawner : MonoBehaviour
{
    public List<GameObject> floors;

    //[SerializeField] private GameObject floorScale;
    [SerializeField] private float floorOffset = 15f; // JAKOB: this could just be public.
                                                      // reasoning is that you change it from outside (the editor)

    // int debugCount = 0;

    void Start()
    {
        // JAKOB: always use {} with conditionals
        // check this:
        //  if (floors != null && floors.Count > 0)
        //      floors = floors.OrderBy(r => r.transform.position.z).ToList();
        //      something = else.ThatLooksLike(it => is.inside.the.conditional).AndItIs(not);
        //
        // better:
        //  if (floors != null && floors.Count > 0) {
        //      floors = floors.OrderBy(r => r.transform.position.z).ToList();
        //      something = else.ThatLooksLike(it => is.inside.the.conditional).AndItIs();
        //  }
        //
        // obviously seems like it's not a big deal,
        // but anything that makes code more intuitive,
        // is gold
        if (floors != null && floors.Count > 0)
            floors = floors.OrderBy(r => r.transform.position.z).ToList();
    }

    public void MoveFloor()
    {
        GameObject movedFloor = floors[0];
        floors.Remove(movedFloor);
        float newZ = floors[floors.Count - 1].transform.position.z + floorOffset;
        movedFloor.transform.position = new Vector3(0, 0, newZ);
        floors.Add(movedFloor);

        // Debug.Log("floor move happened " + debugCount++);
    }
}
