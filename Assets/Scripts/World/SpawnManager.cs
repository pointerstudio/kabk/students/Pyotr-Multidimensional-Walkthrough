using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// JAKOB: I see that the SpawnManager is attached
// to both "Model" and "SpawnManager".
// you can remove it from "Model", as only the one
// attached to "SpawnManager" really does something
public class SpawnManager : MonoBehaviour
{
    FloorSpawner floorSpawner;
    EnemySpawner enemySpawner;
    ObstacleSpawner obstacleSpawner;

    void Start()
    {
        floorSpawner = GetComponent<FloorSpawner>();
        enemySpawner = GetComponent<EnemySpawner>();
        obstacleSpawner = GetComponent<ObstacleSpawner>();
    }

    public void SpawnTriggerEntered()
    {
        floorSpawner.MoveFloor();
        enemySpawner.AwakeTheEvil(); // JAKOB: love the function name, though don't
                                     // enemy and obstacle spawn potentially at the
                                     // same place? could this lead to issues? If both
                                     // have a collider/rigidbody, they might push each
                                     // other into the endless void
        obstacleSpawner.SpawnObstacles();
    }

}
