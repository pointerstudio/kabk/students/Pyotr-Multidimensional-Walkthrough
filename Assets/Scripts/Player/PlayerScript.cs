using System.Collections;
using System.Collections.Generic;
using MoreMountains.Feedbacks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour

{
    Rigidbody m_Rigidbody;

    // health
    public int playerHealth = 100;
    public bool playerDead = false;

    //ammo
    public int currentAmmo;
    public int maxAmmo = 35;
    public int minAmmo = 0;
    //public Text = ammoDisplay;

    // score
    public int deadEnemies = 0;
    public int scoreMultiplier = 0;

    public void Start()
    {

        // JAKOB: if this is the only thing you do with
        // the rigidbody, do it in the editor
        // reasoning: it always happens on start and is never changed
        // so it does not have to be dynamic in a script.
        // if you to it in the editor, then you have less code,
        // and less code means less bugs :-)
        // check in the rigidbody properties under "Constraints"
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Rigidbody.freezeRotation = true;

        //reload the gun at the start
        currentAmmo = maxAmmo;
    }

    public void TakeDamage(int damageAmount)
    {
        playerHealth -= damageAmount;
    }

    void Update()
    {
        if (playerHealth <= 0 && playerDead == false)
        {
            playerDead = true;
            SceneManager.LoadScene("NonGame");
            //Destroy(gameObject);
        }
    }
}
