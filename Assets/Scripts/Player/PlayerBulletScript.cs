using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBulletScript : MonoBehaviour
{
    // JAKOB: you do want to make sure, that this bullet is destroyed
    // either you make it collide with something thick, or you
    // could also take its own position and destroy it when it's
    // far away, or you set a timer to destroy it
    //
    // a timer might work like this:
    //
    // public float destroyAfterNSeconds = 5;
    // float startTime;

    // void Start()
    // {
    //    startTime = Time.time;
    // }

    // void Update()
    // {
    //     if (Time.time > startTime + destroyAfterNSeconds) {
    //         Destroy(gameObject);
    //     }
    // }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Enemy"))
        {
            // values of damage intake can be modified through power economy
            collision.gameObject.GetComponent<EnemyScript>().TakeDamage(5);

            Destroy(gameObject);
        }

        else
        {
            // ????????????????????????
            // should colide with something THICK
            // ????????????????????????

            Destroy(gameObject, 1.0f);
        }
    }
}
